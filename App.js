

import React, {Component} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  Image,
  TouchableOpacity,
  Button,
  FlatList,
} from 'react-native';

import ConexionFetch from './app/components/conexionFetch/ConexionFetch';

import Message from './app/components/message/Message';

import Body from './app/components/body/Body';

import OurFlatList from './app/components/ourFlatList/OurFlatList';
// imports lab07---------
import TransferenceFirst from './app/components/transferences/TransferenceFirst';
import TransferenceSecond from './app/components/transferences/TransferenceSecond';
import TransferenceThird from './app/components/transferences/TransferenceThird';
// imports lab07---------

//import 'react-native-gesture-handler';

import {NavigationContainer} from '@react-navigation/native';

import {createStackNavigator} from '@react-navigation/stack';
//Lab06
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';

import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import {createMaterialBottomTabNavigator} from '@react-navigation/material-bottom-tabs';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
//crear la instancia
//const Tab = createBottomTabNavigator();
//const Tab = createMaterialBottomTabNavigator();


//--------lab07------------------------
const TransferenceStack = createStackNavigator();

function TransferenceStackScreen(){
    return(
        <TransferenceStack.Navigator>
            <TransferenceStack.Screen name="Datos generales" component={TransferenceFirst}/>
            <TransferenceStack.Screen name="Informacion" component={TransferenceSecond}/>
            <TransferenceStack.Screen name="Completado" component={TransferenceThird}/>
        </TransferenceStack.Navigator>
   )
}
//-------------------------------------


const Tab = createMaterialTopTabNavigator();

function HomeScreen({navigation}) {
  return (
    <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
      <Text>Home Screen</Text>
      <Button
        title="Go to Details"
        onPress={() => navigation.navigate('Details')}
      />
    </View>
  );
}

function DetailsScreen({navigation}) {
  return (
    <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
      <Text>Details Screen</Text>

      <Button
        title="Go to Details... again"
        onPress={() => navigation.push('Details')}
      />
      <Button title="Go to Home" onPress={() => navigation.navigate('Home')} />

      <Button title="Go back" onPress={() => navigation.goBack()} />
      
      <Button
        title="Go back to first screen in stack"
        onPress={() => navigation.popToTop()}
      />
    </View>
  );
}

/*
const Stack = createStackNavigator();

const provincias = [
  {
    id: 1,
    name: 'Arequipa',
  },
  {
    id: 2,
    name: 'Puno',
  },
  {
    id: 3,
    name: 'Cuzco',
  },
];
*/
function Item({title, image}) {
  return (
    <View style={styles.item}>
      <Image source={{uri: image}} style={{height: 40, width: 40}} />
      <Text style={styles.title}>{title}</Text>
    </View>
  );
}

export default class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      textValue: 0,
      count: 0,
      items: [],
      error: null,
    };
  }

  componentDidMount() {
    console.warn('Estamos dentro componentDidMount', this.state.textValue);
  }

  shouldComponentUpdate() {
    console.warn('Estamos dentro shouldComponentUpdate', this.state.error);
  }

  changeTextInput = text => {
    this.setState({textValue: text});
  };

  onPress = () => {
    this.setState({
      count: this.state.count + 1,
    });
  };

  /*render() {
    return <ConexionFetch />;
  }*/

  /*render() {
    return <OurFlatList />;
  }*/

  /*
  render() {
    return (
      <NavigationContainer>
        <Stack.Navigator>
          <Stack.Screen name="Home" component={HomeScreen} />
          <Stack.Screen name="Details" component={DetailsScreen} />
        </Stack.Navigator>
      </NavigationContainer>
    );
    }
    */

    //PARTE 1 DEL LAB 6
    /*
  render() {
    return (
      <NavigationContainer>
        <Tab.Navigator>
          <Tab.Screen name="Home" component={HomeScreen} />
          <Tab.Screen name="Details" component={DetailsScreen} />
        </Tab.Navigator>
      </NavigationContainer>
    );
  }
  */

  render(){
    return(
      <NavigationContainer>
        <Tab.Navigator
        initialRouteName="Home"
        tabBarOptions={{
          activeTintColor:'#e91e63',
        }}
        >
          <Tab.Screen
            name ="Home"
            component={HomeScreen}
            options={{
              tabBarLabel:'Home',
              tabBarIcon: ({color, size}) =>(
                <MaterialCommunityIcons name="home" color={color} size={size} />
              ),
            }}
          />

          <Tab.Screen
            name="Transference"
            component={TransferenceStackScreen}
            options={{
                tabBarLabel: 'Transference',
                tabBarIcon: ({color, size})=>(
                    <MaterialCommunityIcons name="home" color={color} size={size}/>
                ),
            }}

          />

          <Tab.Screen
            name ="Details"
            component={DetailsScreen}
            options={{
              tabBarLabel:'Details',
              tabBarIcon: ({color, size}) =>(
                <MaterialCommunityIcons name="bell" color={color} size={size} />
              ),
            }}
          />

        </Tab.Navigator>
      </NavigationContainer>
    );
  }
  
}

const styles = StyleSheet.create({
  text: {
    alignItems: 'center',
    padding: 10,
  },

  button: {
    top: 10,
    alignItems: 'center',
    backgroundColor: '#DDDDDD',
    padding: 10,
  },
  countContainer: {
    alignItems: 'center',
    padding: 10,
  },
  countText: {
    color: '#FF00FF',
  },

  container: {
    flex: 1,
    marginTop: 20,
  },
  item: {
    backgroundColor: 'orange',
    padding: 20,
    marginVertical: 8,
    marginHorizontal: 16,
  },
  title: {
    fontSize: 32,
  },
});
