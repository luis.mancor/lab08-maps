var moment = require('moment');
moment.locale('es');

import React, {Component} from 'react';
import {Text, View, Button, StyleSheet, Image} from 'react-native';

export default class TransferenceThird extends Component{
    constructor(props){
        super(props);
        this.state={
            importe: 0,
        };
    }

    render(){
        return(
            <View style={styles.container}>
                <Text style={{marginBottom:23, alignSelf:'center'}}>Informacion enviada con exito!</Text>
                <Image style={{alignSelf:'center'}} source={require('../../img/imagen.png')}/>
                <Button title="Aceptar" onPress={() => this.props.navigation.navigate('Datos generales')} />
            </View>
        )
    }
//

}

const styles = StyleSheet.create({
    titleInput: {
      //flex: 1,
      //marginTop: 20,
      //backgroundColor: 'green',
    },
    viewInput:{
        backgroundColor: '#CCE1D5',
    },
    picker:{
        backgroundColor: '#DCCCDB',
        width: 480,

    },
    pickerModal:{

    },
    viewCalendar:{
    },
    container:{
        margin: 15,
        //backgroundColor:'#DDDDDD',
    },
    buttonStyle:{
        justifyContent: 'center',
        alignItems:'center',
        alignSelf:'center',
        margin:30,
    },
  });